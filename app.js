const http = require('http');
const https = require('https');
const bodyParser = require('body-parser');
const express = require('express');
const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
var schedule = require('node-schedule');
const rzInfo = require('./src/utils/rzInfo');
const config = require('./config');
const log = require('./src/utils/logger');
const swaggerUi = require('swagger-ui-express');
const fs = require('fs-extra');

schedule.scheduleJob('0 0 * * *', async () => {
  await require ('./src/routines/reloadDataModels').updateDataModels();
  await require ('./src/routines/updateDataModels')(); 
}); 

// schedule.scheduleJob('* 50 23 1 * *', async () => {
//   await require ('./src/routines/updateNgsiApi')(); 
// });

// schedule.scheduleJob('* 50 23 1 * *', async () => {
//   await require ('./src/routines/updateHistoricalApi')(); 
// });

process.env.NODE_ENV = config.env; // 'debug' or 'production' for the logger
process.env.LOG = config.logLevel; 
process.env.MODE = config.mode; // 'commandLine' or 'server'
process.env.SUPPRESS_NO_CONFIG_WARNING = 'y';
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

const dbAddr = config.dbAddr || 'localhost';
const dbPort = config.dbPort || 27017;
const dbName = config.dbName || 'scCityStatus';

mongoose.connection.on('open', async (ref) => {
  log.info('Connected to mongo db!');
  return setupServer();
});

mongoose.connection.on('error', (err) => {
  log.error('Could not connect to mongo db!');
  process.exit(-1);
});

const setupService = async () => {
  try {
    const db = 'mongodb://' + dbAddr + ':' + dbPort + '/' + dbName;
    log.info('Started connection on [' + db + '] waiting for it to open...');
    await mongoose.connect(db);        
  } catch (e) {
    log.error('Setting up failed to connect to [' + db + '], ' + err.message);
  }
}

const setupServer = async () => {  
  const app = express();
  const router = express.Router();

  app.use(bodyParser.urlencoded({ extended: true }));  
  app.use(bodyParser.json());

  app.use( async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Authorization');
    return next();
  });
  app.use('/', router);
  const swaggerDocument = require('./docs/swagger.json');
  app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
  // app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
  require('./src/routes/dataModels.js') (app); 
  require('./src/routes/rzsStatus.js') (app); 
  // require('./src/routes/valDms.js') (app); 
  // require('./src/routes/valHis.js') (app); 
  // require('./src/routes/valNgsi.js') (app); 
  // require('./src/routes/dev.js') (app); 

  if (process.env.NODE_ENV === 'debug'){
    const httpPort = config.httpPort || 8080;
    const httpServer = http.createServer(app); 
    httpServer.listen(httpPort);
    log.info('HTTP Server listening in PORT ' + httpPort); 
  } else {
    const httpsPort = config.httpsPort || 443;
    var options = { 
      key: fs.readFileSync(config.httpsKey), 
      cert: fs.readFileSync(config.httpsCert)
    };
    var httpsServer = https.createServer(options, app);
    httpsServer.listen(httpsPort);
    log.info('HTTP Server listening in PORT ' + httpsPort); 
  }
}

const callUp = async () => { 
  await setupService(); 
  
  // await require ('./src/routines/updateNgsiApi')();
  // await require ('./src/routines/updateHistoricalApi')();
  // await require ('./src/routines/reloadDataModels').updateDataModels();
  // await require ('./src/routines/updateDataModels')(); 

  // const tok = await require ('./src/utils/rzInfo').getToken(); 
  // console.log(tok)
  // await require ('./src/routines/updateDataModels')(); 
  // await require ('./src/routines/updateHistoricalApi')();
}
callUp();