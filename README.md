# SynchroniCity MIM validator 

This is a service to validate the interoperability points defined in the Synchronicity project. 

It permits to carry out periodic against preconfigured endpoints and punctual validations.

In particular it validates:

* NGSI API and that data in instances of [Orion Context Broker](https://fiware-orion.readthedocs.io/en/master/)
* Data models defined within the [SynchroniCity Project](https://gitlab.com/synchronicity-iot/synchronicity-data-models).  It leverages [AJV JSON Schema Validator](https://github.com/epoberezkin/ajv).
* Historical API defined in the [SynchroniCity Project](https://synchronicity-iot.eu/)
* OAUTH2 interface <span style="color:red">[under definition]</span>

The service is developed in [Node.js](https://nodejs.org/es/) and is used as a REST service. Results of periodic valition are stored in a [Mongo](https://www.mongodb.com/es) database

## Global configuration
The global setup is defined in the ``config.js`` file with the following options:

* ``config.env``: if set to *production* it creates logs in the corresponding files. Otherwise the logging messages are displayed in the standard output.
* ``config.logLevel``: logging level for [WINSTON](https://www.npmjs.com/package/winston).
* ``config.queryLimit``: it defines the maximum amount of context  entities of one type queried during the validation. Defatul *100000*.
* ``config.queryStep``: it defines the maximum amount of context  entities queried at once. Default *500*. 
* ``config.httpPort``: listening port. Default *8080*.
* ``config.dbAddr``: address of the database. Default: *localhost*.* ``config.dbPort``: listening port of the database. Default *27017*.
* ``config.dbName``: database where the output of the periodic validations are stored. Default: *scCityStatus*.
* ``config.dataModelsRepo``: repository of SynchroniCity datamodels. (*https://gitlab.com/synchronicity-iot/synchronicity-data-models.git*). Do not change it.
* ``config.fwDataModelsRepo`` = repository of Fiware data models(*https://github.com/Fiware/dataModels.git*). Do not change it.

##  Installation 
The validator has been developed and tested using nodejs version 11.4.0. You can manage the nodejs version using [npm](https://nodejs.org/en/download/package-manager/).

Once nodejs installed in your system, just go to the validator folder
```
cd rz-instance-validator
```
And install the dependencies
```
npm install
```
Finally you can run the service manually
```
node app.js
```
or using [pm2](http://pm2.keymetrics.io/) to keep it running
```
pm2 start pm2.json
```

## Configuration

To perform the periodic validation, it is necessary to provide some pieces of information of the SynchroniCity instances to be validated. 

This configuration is tobe stored in `rz-instance-validator/src/utils/rzInfo.js`. In the repository, there is a documented example in `rz-instance-validator/src/utils/rzInfo.js.example`, that whows all the possible paramters that need to be defined.

##  Interface description

The validator can be used in two different ways:
* Manual check of interfaces and data
* Access to periodic validation

You can find further information in [SWAGGER](https://framework-validator.synchronicity-iot.smartsantander.eu/api-docs/)

## SynchroniCity defined datamodels API

This API is just to check that datamodels that are defined:

* ``/dataModel/list``: returns a list of compiled datamodels
* ``/dataModelDoc/list``: returns references to data models documentation
* ``/dataModel/list/{DATA_MODEL_NAME}``: returns the defined schema JSON for the datamodel

## Access to periodic validation

### NGSI API

* ``/cities/ngsiApi``: returns the cities for which there is periodic NGSI API validation
* ``/cities/{CITY_NAME}/ngsiApi``: returns the current status of the NGSI API validation for a given city

### Datamodels API

* ``/cities/dataModels``: returns the cities/service/service-path for which there is periodic datamodel validation
* ``/cities/{CITY_NAME}/dataModels/list``: returns the type of datasets of a given city/service/service-path
* ``/cities/{CITY_NAME}/dataModels``: returns the validation of a given city/service/service-path
* ``/cities/{CITY_NAME}/dataModels/{DATAMODEL_NAME}``: returns the validation of one dataset of a given city/service/service-path

### Historical API

* ``/cities/historicalApi``: returns an array of the validated cities and the last time they were updated
* ``/cities/{CITY_NAME}/historicalApi``: returns the current status of the historical API validation for a given city


## Online validation 

* ``/online/ngsiApi``: performs the NGSI API validation for an enpoint
* ``/online/historicalApi``: performs the historical API validation for an enpoint
* ``/online/dataModel``: performs the data models validation for an enpoint
* ``/online/dataModel/{DATAMODEL_NAME}``: performs the data model validation for a given datamodel for an enpoint
