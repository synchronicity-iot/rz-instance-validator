const _ = require('lodash');
const validate = require('../checks/doValidate');
const errMsg = require('../utils/errors');
const log = require('../utils/logger');
const validateNgsiApi = require('../utils/common').validateNgsiApi;

const ngsiApi = async (req, res) => {
  const url = req.body.url;
  const fs = req.body.fiwareService ||'';
  const fsp = req.body.fiwareServicePath ||'';
  const auth = req.body.authHdr || {};

  if (!url) {
    return res.status(400).send(errMsg.urlUnspec);
  }
  const opt = {
    url : url,
    headers: auth
  }
  opt.headers['fiware-service'] = fs;
  opt.headers['fiware-servicePath'] = fsp;


  const aux = await validateNgsiApi(opt).catch (e => {
    return res.status(400).send();  
  });
  let ret = {
    url : url,
    fiwareService: fs,
    fiwareServicePath: fsp
  };

  ret = _.merge(ret, aux);
  return res.send(ret);
};

module.exports = (app) => {
  app.post('/online/ngsiApi', ngsiApi);
}; 