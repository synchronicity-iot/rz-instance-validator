const log = require('../utils/logger');
const validate = require('../checks/checkEnts').validate;

module.exports = async (app) => {
  app.post('/test', async (req, res) => {

    const ret = await validate (req.body, req.body.type);

    return res.status(201).send(ret);
  });
};