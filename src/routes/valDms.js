const _ = require('lodash');
const validate = require('../checks/doValidate');
const errMsg = require('../utils/errors');
const log = require('../utils/logger');
const ngsiApi = require('../apis/ngsiApi');
const getErrorMsg = require('../utils/errors').getErrorMsg;
const validateProvider = require('../utils/common').validateProvider;

const validateDataModels =  async (req, res) => {
  const url = req.body.url;
  const fs = req.body.fiwareService || '';
  const fsp = req.body.fiwareServicePath || '';
  const detail = req.query.detail || 'true';
  const auth = req.body.authHdr ||{};
  
  if (!url) {
    return res.status(400).send(errMsg.urlUnspec);
  }   

  const opt = {
    url : url,
    headers: auth
  }
  opt.headers['fiware-service'] = fs;
  opt.headers['fiware-servicePath'] = fsp;
  
  let ret = {
    url: url,
    fiwareService: fs,
    fiwareServicePath: fsp
  };

  
  try {
    const d = await validateProvider(opt, true).catch (getErrorMsg);
    if (detail === 'false') {
      for (const key in d) {
        ret[key] = {success: d[key].success, fail: d[key].fail};
      }
    } else {
      ret = _.merge(ret, d);
    }
    return res.status(200).send(ret);
  }
  catch (e) {
    return res.status(400).send();  
  }
};

const validateDataModelsType =  async (req, res) => {
  const url = req.body.url;
  const fs = req.body.fiwareService || '';
  const fsp = req.body.fiwareServicePath || '';
  const auth = req.body.authHdr ||{};
  const type = req.params.type;
  
  if (!url) {
    return res.status(400).send(errMsg.urlUnspec);
  }   

  const opt = {
    url : url,
    headers: auth
  }
  opt.headers['fiware-service'] = fs;
  opt.headers['fiware-servicePath'] = fsp;
  
  let ret = {
    url: url,
    fiwareService: fs,
    fiwareServicePath: fsp
  };
  try {
    d = await validate.entitiesByType(opt, type, true);
    const aux= {};
    aux[type] = d;
    ret = _.merge(ret, aux);
    return res.status(200).send(ret);
  } catch (e) {
    return res.status(400).send();  
  }  
};


module.exports = (app) => {
  app.post('/online/dataModels', validateDataModels);
  app.post('/online/dataModels/:type', validateDataModelsType);
};