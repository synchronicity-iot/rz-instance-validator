const _ = require('lodash');
const validate = require('../checks/doValidate');
const log = require('../utils/logger');
const errMsg = require('../utils/errors');
const validateHistApi = require('../utils/common').validateHistApi;

const validateHistoricalApi = async (req, res) => {
  const url = req.body.url;
  const fs = req.body.fiwareService ||'';
  const fsp = req.body.fiwareServicePath ||'';
  const auth = req.body.authHdr || {};
  
  const type = req.body.entityType;
  const entityId = req.body.entityId;
  const attribute = req.body.attribute;
  
  if (!url) {
    return res.status(404).send(errMsg.urlUnspec);
  }
  if (!entityId) {
    return res.status(404).send(errMsg.entityUnspec);
  }
  if (!attribute) {
    return res.status(404).send(errMsg.attributeUnspec);
  }

  const opt = {
    url : url,
    headers: auth
  }
  opt.headers['fiware-service'] = fs;
  opt.headers['fiware-servicePath'] = fsp;
  
  const aux = await validateHistApi(opt,  {id: entityId, type: type}, attribute).catch (e => {
    console.log(e)
    return res.status(404).send();  
  });
  let ret = {
    url: url,
    fiwareService: fs,
    fiwareServicePath: fsp,
    entityType: (type ? type : null),
    entityId: entityId,
    attribute: attribute,
  };
  ret = _.merge(ret, aux);
  res.status(200).send(ret);
}

module.exports = (app) => {
    app.post('/online/historicalApi', validateHistoricalApi);
};