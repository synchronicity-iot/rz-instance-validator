const _ = require('lodash');
const dmsModel = require('../models/dmsValidationModel');

const getFiwareService = (req) => {
  return  req.headers['fiware-service'] ? req.headers['fiware-service'] : "";
}

const getFiwareServicePath = (req) => {
  return  req.headers['fiware-servicePath'] ? req.headers['fiware-servicePath'] : 
          req.headers['fiware-servicepath'] ? req.headers['fiware-servicepath'] : 
          req.query['fiware-servicePath'] ? req.query['fiware-servicePath'] : 
          req.query['fiware-servicepath'] ? req.query['fiware-servicepath'] : 
          "";
}

const getFiwareEndpoint = (req) => {
  return  req.headers['fiware-endpoint'] ? req.headers['fiware-endpoint'] : 
          req.headers['fiware-endPoint'] ? req.headers['fiware-endPoint'] : 
          null;
}

const rzsDms = async (req, res) => {
  const aux = await dmsModel.distinct('city');
  return res.send(aux);
};

const cityServices =  async (req, res) => {
  const ret = await dmsModel.find({city: req.params.city.toLowerCase()}, '-_id')
  .select('city service servicePath endpoint lastUpdate');
  return res.send(ret);
}

const summaryCityDataModels = async (req, res) => {
  const city = req.params.city;
  const services = await dmsModel.find({city: city.toLowerCase()});

  const ret = {}
  const retServ = []
  for (service of services) {
    _.each (service.status, (v,k) => {
      if (_.has(ret, k)) {
        ret[k].total += v.total;
        ret[k].success += v.success;
        ret[k].fail += v.fail;
      } else {
        ret[k] = {}
        ret[k].total = v.total;
        ret[k].success = v.success;
        ret[k].fail = v.fail;
      }
    })
    retServ.push(_.pick(service, ['city', 'service', 'servicePath', 'endpoint']));
  }
  return res.send({services: retServ, dataStatus: ret });
}

const statusCityDataModels = async (req, res) => {
  const fs = getFiwareService(req);
  const fsp = getFiwareServicePath(req);         
  const ep = getFiwareEndpoint(req);         
  const city = req.params.city;
  const query = {
    city: city,
    service: fs,
    servicePath: fsp,
    endpoint: ep  
  }
  const dmsVal = await dmsModel.findOne (query, '-_id _v').select( ['city', 'service', 'servicePath', 'endpoint', 'status', 'lastUpdate']);   
  if(!dmsVal){
    return res.status(404).send({
      msg: 'Data models validation not found',
      city: city,
      fiwareService: fs,
      fiwareServicePath: fsp,
      endpoint: ep
    });  
  }
  return res.send(dmsVal);
}

module.exports = (app) => {
  app.get('/cities/dataModels', rzsDms);
  app.get('/cities/:city/services', cityServices);
  app.get('/cities/:city/dataSummary', summaryCityDataModels);
  app.get('/cities/:city/data', statusCityDataModels);
};