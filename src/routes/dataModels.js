const checkEnts = require('../checks/checkEnts');
const loadDms  = require('../routines/reloadDataModels');

const dataModelNames = async (req, res) => {
  return res.send(checkEnts.getSchemaNames());
}

const dataModel = async (req, res) => {
  
  const dm = checkEnts.getSchema(req.params.dm);
  if (!dm) {
    return res.status(404).send();
  }
  return res.send(dm);
}

const dataModelDoc = async (req, res) => {
  return res.send(loadDms.getDocumentation());
}

const dataModelDocModel = async (req, res) => {
  const docs = loadDms.getDocumentation();
  const ret = docs[req.params.dm];
  if (ret !== null) {
    return res.send(ret);
  }
  res.status(404).send()
}

module.exports = (app) => {
  app.get('/dataModel/list', dataModelNames);
  app.get('/dataModel/:dm', dataModel);
  // app.get('/dataModelDoc/list', dataModelDoc);
  // app.get('/dataModelDoc/:dm', dataModelDocModel);

};