const _ = require('lodash');
const rzInfo = require('../utils/rzInfo'); 
const StatusModel = require('../models/dmsValidationModel');
const log = require('../utils/logger');
const getErrorMsg = require('../utils/errors').getErrorMsg;
const validateProvider = require('../utils/common').validateProvider;

const updateService = async (city ,fs, fsp, ep, d) => {
  let item = {
    city: city.toLowerCase(),
    service: fs.toLowerCase(),
    servicePath: fsp.toLowerCase(),
    endpoint: ep.toLowerCase(),
    status: d,
    lastUpdate: Date.now()
  };
  await StatusModel.create(item).catch (async e => {
    await StatusModel.update(item).catch (async e => {});
  });
  return;
}

const updateCity = async (cityName) => {
  log.info('========== Data models ' + cityName)
  for (scInstance of rzInfo[cityName].dmsValidation){
    const authHdr = await scInstance.getAuthHdr();
    const cdEndpoint = scInstance.endpoint;
    const opt = { url : cdEndpoint, headers: authHdr }
    opt.headers.Accept = 'application/json';
    const servPaths = scInstance.fiwareServicePaths
    for (const fs of servPaths) {
      opt.headers['fiware-service'] = fs[0];    
      opt.headers['fiware-servicePath'] = fs[1];    
      log.info('Validating data models of ' + cdEndpoint + ' @ ['+ fs[0] + '' + fs[1] + ']');
      const d = await validateProvider(opt, true).catch (getErrorMsg);
      updateService (cityName, fs[0], fs[1], cdEndpoint, d);
    }
  }
  log.info('=================================')
} 

const updateAll = async () => {
  for (const key in rzInfo) {
    if (_.includes(rzInfo[key].toValidate, 'dataModels') && _.has(rzInfo[key], 'dmsValidation')) {
      await updateCity(key);
    } else {
      log.warn('----------------- No datamodel validation for ' + key)
    }
  }
}

module.exports = updateAll;