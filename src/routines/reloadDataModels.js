const git = require("git-promise");
const fs = require('fs-extra');
const log = require('../utils/logger');
const repo = require('../../config').dataModelsRepo;
const fwRepo = require('../../config').fwDataModelsRepo;
const md2json = require('md-2-json');
const cmd=require('cmd-promise');
const recompileDms = require('../checks/checkEnts').recompileDms;
const tb2json = require('mdtable2json')

let documentation = {}

const parseDocFiles = async (d) => {
  documentation = {};
  let data2 = d.split('\n');
  data2 = data2.slice(7, -2);
  for (const l of data2) {
    const aux = l.split('|');

    const name = aux[2].replace(/ *\([^)]*\) */g, "");
    const doc = aux[2].match(/\((.*)\)/);
    if(doc !== null) {
      documentation[name] = doc[1];
    }
  }
}

const parseReadme = async () => {
  let  data = await fs.readFile( './DataModels/README.md', 'utf8').catch((e) => {
    process.exit(-1);
  });
  data = md2json.parse(data)['SynchroniCity Data Models']['List of SynchroniCity Data Models'].raw;
  
  data = data.replace(/[\*\[\]]+/g, '');
  parseDocFiles(data);
  
  data = data.replace(/ *\([^)]*\) */g, "");
  data = tb2json.getTables(data);
  
  return data[0].json;
}

const syncSc = async () => {
  if (fs.existsSync('./DataModels')) {
    log.info ('SC data models folder is in place. Updating...');
    const ret = await git('git -C ./DataModels  pull');
    log.info (ret);
  } else {
    log.info ('Cloning SC data models');
    log.info(repo) 
    const ret = await git('git clone ' + repo + ' ./DataModels');
    log.info (ret);
    log.info("SC clone finished");
  }
} 

const syncFw = async () => {
  if (fs.existsSync('./FwDataModels')) {
    log.info ('Fiware Data models folder is in place. Updating...');
    const ret = await git('git -C ./FwDataModels  pull');
    log.info (ret);
  } else {
    log.info ('Fiware cloning Fiware data models');
    log.info(fwRepo) 
    const ret = await git('git clone ' + fwRepo + ' ./FwDataModels');
    log.info (ret);
    log.info("Fiware clone finished");
  }
}

const removeRepos = async () => {
  log.info ('Removing existing repo');
  await cmd(' rm -rf ./DataModels');
}

const mergeAndClean = async () => {
  await cmd(' cp -rn ./FwDataModels/specs/* ./DataModels').catch ((e) => {
  }); 
  await cmd(' cp -f ./FwDataModels/geometry-schema.json ./DataModels/geometry-schema.json').catch ((e) => {
  }); 
  await cmd(' cp -f ./FwDataModels/common-schema.json ./DataModels/common-schema.json').catch ((e) => {
  }); 
  await cmd(' rm -rf ./FwDataModels');
}

const removeSchemas = async () => {
  log.info ('Removing existing repo');
  await cmd(' rm ./src/schemas/*.json').catch(e => {
    log.warn('Empty schemas folder');
  });
}

const updateDataModels = async () => {
  await removeRepos();
  await syncSc();
  await syncFw();
  await mergeAndClean();
  await removeSchemas();
  const dms = await parseReadme();
  await recompileDms(dms);
}

module.exports = {
  updateDataModels: updateDataModels,
  getDocumentation : () => {return documentation;}
};