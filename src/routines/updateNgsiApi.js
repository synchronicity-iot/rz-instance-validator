const _ = require('lodash');
// const rzInfo = require('../utils/rzInfo'); 
// const rzInfo = require('../utils/rzInfoAntwerp'); 
// const rzInfo = require('../utils/rzInfoEindhoven'); 
const rzInfo = require('../utils/rzInfoHelsinki'); 
const StatusModel = require('../models/ngsiValidationModel');
const log = require('../utils/logger');
const getErrorMsg = require('../utils/errors').getErrorMsg;
const validateNgsiApi = require('../utils/common').validateNgsiApi;

const updateCity = async (cityName) => {
  if (!_.has(rzInfo, cityName)) {
    log.error('Unknown city ' + cityName)
    return;
  }
  log.info('Updating data NGSI API for city ' + cityName)
  
  try {
    const authHdr = await rzInfo[cityName].getAuthHdr();
    const cdEndpoint = rzInfo[cityName].contextData;
    const opt = {
      url : cdEndpoint,
      headers: authHdr
    } 
    opt.headers.Accept = 'application/json';

    log.info(opt)
    let model = new StatusModel({
      _id: cityName.toLowerCase(),
      lastUpdate: Date.now(),
      status: {}, 
    }, { _id: false });
    model.status = await validateNgsiApi(opt).catch (getErrorMsg);
    console.log(model.status)
    await StatusModel.update({_id: model._id}, model, {upsert: true}).catch ((e) => { return;});
  } catch (e) { }
} 

const updateAll = async () => {
  for (const key in rzInfo) {
    if (_.includes(rzInfo[key].toValidate, 'ngsiApi')) {
      await updateCity(key);
    } else {
      log.info('No NGSI validation for ' + key)
    }
  }
}

module.exports = updateAll;