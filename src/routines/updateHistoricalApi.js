const _ = require('lodash');
const rzInfo = require('../utils/rzInfo'); 
const StatusModel = require('../models/histValidationModel');
const log = require('../utils/logger');
const getErrorMsg = require('../utils/errors').getErrorMsg;
const validateHistApi = require('../utils/common').validateHistApi;

const updateCity = async (cityName) => {
  log.info('Updating historical API for city ' + cityName)
  const authHdr     = await rzInfo[cityName].getAuthHdr();
  const hdEndpoint  = rzInfo[cityName].histData;
  const entityId    = rzInfo[cityName].histValidation.entityId;
  const attribute   = rzInfo[cityName].histValidation.attribute;
  const fs          = rzInfo[cityName].histValidation.fiwareService;
  const fsp         = rzInfo[cityName].histValidation.fiwareServicePath;
  const type        = rzInfo[cityName].histValidation.type;
  
  const opt = {
    url : hdEndpoint,
    headers: authHdr
  }
  opt.headers.Accept = 'application/json';
  opt.headers['fiware-service'] = fs;
  opt.headers['fiware-servicePath'] = fsp;
  log.info ('Entity/attribute --> ' + entityId + '/' + attribute);  
  log.info ('FiwareService/FiwareServicePath/type --> ' + fs + ' / ' + fsp + '/'+ (type ? type : ' '));  
  let model = new StatusModel({
    _id: cityName.toLowerCase(),
    lastUpdate: Date.now(),
    entityId: entityId, 
    attribute: attribute,
    fiwareService: fs.toLowerCase(),
    fiwareServicePath: fsp.toLowerCase(),
    entityType: (type ? type : null),
    status: {}
  }, { _id: false });

  model.status = await validateHistApi(opt, {id: entityId, type: type}, attribute);
  await StatusModel.update({_id: model._id}, model, {upsert: true}).catch ((e) => { return;});
} 

const updateAll = async () => {
  for (const key in rzInfo) { 
    if (_.includes(rzInfo[key].toValidate, 'historicalApi') && 
      _.has(rzInfo[key], 'histValidation') && 
      _.has(rzInfo[key].histValidation, 'entityId') && 
      _.has(rzInfo[key].histValidation, 'attribute') && 
      _.has(rzInfo[key].histValidation, 'fiwareService') && 
      _.has(rzInfo[key].histValidation, 'fiwareServicePath')
    ) {
      await updateCity(key);
    } else {
      log.warn('No historical API validation for ' + key)
    }
  }
}

module.exports = updateAll;