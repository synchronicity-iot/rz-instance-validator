const log = require('./logger');
const axios = require('axios');

const getCalatayudToken = async () => {
  const options = {
    method: 'post',
    url: 'https://ocb-swm.wtelecom.es/oauth2/token',
    data: 'grant_type=client_credentials&client_id=fe5c8b5e-2d52-4c0c-9feb-0c18c80c11dc&client_secret=f6d4f8f6-9c51-46d9-973a-1812a5031369',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  };
  let res = await axios(options).catch (e=> {console.log(e.response.data)});
  if (res) {
    return {'Authorization': 'Bearer ' + res.data['access_token']};
  } else {
    return undefined
  }
};


module.exports = {
  getToken : getCalatayudToken,
  Calatayud :  { 
    contextData : 'https://ocb-swm.wtelecom.es',
    getAuthHdr: getCalatayudToken,
    toValidate: ['dataModels']
  }

}
