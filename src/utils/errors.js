const _ = require('lodash');

const urlUnspec = {
  'type': '',
  'title': 'URL unspecified',    
  'detail': 'You need to specify the URL the context broker is listening at including the protocol'
};

const typeUnspec = {
    'type': '',
    'title': 'Type unspecified',    
    'detail': 'You need to specify the TYPE of the entities you are validating'
};

const entityUnspec = {
  'type': '',
  'title': 'Entity unspecified',    
  'detail': 'You need to specify the entity ID'
};

const attributeUnspec = {
  'type': '',
  'title': 'Attribute unspecified',    
  'detail': 'You need to specify attribute name'
};

const getErrorMsg = (e) => {
  try {
    return {url: e.response.config.url, status: e.response.status, msg: e.response.statusText, data: e.response.data};    
  } catch (er) {
    try {
      return {url: e.config.url, status: 'unknown error'};    
    } catch (er) {
      return 'unknown error';    
    }
  }
}

const getErrorMsg2 = (e) => {
  try {
    return {url: e.config.url, status: e.response.status, msg: e.response.statusText};    
  } catch (e) {
    return 'unknown error';
  }
}

module.exports = {
  urlUnspec: urlUnspec,
  typeUnspec: typeUnspec, 
  entityUnspec: entityUnspec, 
  attributeUnspec: attributeUnspec, 
  getErrorMsg: getErrorMsg,
  getErrorMsg2: getErrorMsg2
};
