const ngsiApi = require('../apis/ngsiApi');
const validate = require('../checks/doValidate');
const getErrorMsg = require('./errors').getErrorMsg;
const log = require('../utils/logger');

const validateProvider = async (opt, v) => {
  const data = {};
  const types = await ngsiApi.getAllTypeNames(opt)
  log.info('Validating....');
  log.info(types)
  for (const type of types) {
  // if (type === 'Device') {  
    const tRet = await validate.entitiesByType(opt, type, v).catch( (e) => {
      log.warn ('Error validating type...');
      console.error(e)
      getErrorMsg(e)
    })
    data[type] = tRet;
  } 
  //}
  return data;
}

const validateHistApi = async (opt, ent, attr) => {
  const d =  await validate.historicalApi(opt, ent, attr);
  return d;
}

const validateNgsiApi = async (opt) => {
  return await validate.ngsiApi(opt);
}

module.exports = {
  validateNgsiApi : validateNgsiApi,
  validateHistApi : validateHistApi,
  validateProvider : validateProvider
}