const winston = require('winston');
const DailyRotateFile = require('winston-daily-rotate-file');
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;
// { error: 0, warn: 1, info: 2, verbose: 3, debug: 4, silly: 5 }

const myFormat = printf(msg => {
  return `[${msg.timestamp} [${msg.label}] ${msg.level}: ${msg.message}`;
});

const transport = new (winston.transports.DailyRotateFile)({
  dirname: './logs',
  level: 'silly',
	filename: 'serv.log',
	datePattern: 'YYYY-MM-DD',
  maxsize: '5mb', //5MB,
  zippedArchive: true
}); 

const transportErr = new (winston.transports.DailyRotateFile)({
  dirname: './logs',
  level: 'error',
	filename: 'error.log',
	datePattern: 'YYYY-MM-DD',
  maxsize: '5mb', //5MB,
  zippedArchive: true
});

logger = winston.createLogger({  
  level: process.env.LOG,
  format: combine(
    label({ label: __filename.replace(/^.*[\\\/]/, '') }),
    timestamp(),
    myFormat
  ),
  transports: [
    transportErr,
    transport    
  ],
});

if (process.env.NODE_ENV !== 'production') {
    logger.add(new winston.transports.Console({
    format: combine(
        winston.format.colorize({message: true}),
        label({ label: 'synchronicity-mim-validator' }),
        timestamp(),
        myFormat
    ),
  }));
}

module.exports = logger;
