const log = require('./logger');
const axios = require('axios');

const getAntwerpPilotsToken = async () => {
  return {'apikey': 'b654a825-5296-41b0-8889-235e9f656b51'};
};


const getAntwerpToken = async () => {
  const options = {
    method: 'post',
    url: 'https://ext-api-gw-p.antwerpen.be/sirus/orion-consumer-api-a/v2/oauth2/token',
    data: 'grant_type=client_credentials&client_id=6aee2b05-4401-4ecf-8f3d-c5c2f29a49ac&client_secret=418ead9f-c315-4cc5-8424-678d77fd62e7',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  };
  let res = await axios(options);
  return {'Authorization': 'Bearer ' + res.data['access_token']};
};


module.exports = {
  // AntwerpActiveTravelInsigths :  { 
  //   contextData : 'https://ext-api-gw-p.antwerpen.be/digipolis/sbascactivetravelis/v1',
  //   getAuthHdr: getAntwerpPilotsToken,
  //   toValidate: ['ngsiApi']
  // },
  // AntwerpEncouragingCycling :  { 
  //   contextData : 'https://ext-api-gw-p.antwerpen.be/digipolis/sbascencouragingcyc/v1',
  //   getAuthHdr: getAntwerpPilotsToken,
  //   toValidate: ['ngsiApi']
  // },
  // AntwerpKissMyBike :  { 
  //   contextData : 'https://ext-api-gw-p.antwerpen.be/digipolis/sbasckissmybike/v1',
  //   getAuthHdr: getAntwerpPilotsToken,
  //   toValidate: ['ngsiApi']
  // },
  // AntwerpKissMyBike :  { 
  //   contextData : 'https://ext-api-gw-p.antwerpen.be/digipolis/sbascrainbrain/v1',
  //   getAuthHdr: getAntwerpPilotsToken,
  //   toValidate: ['ngsiApi']
  // },
  // AntwerpStreetLights :  { 
  //   contextData : 'https://ext-api-gw-p.antwerpen.be/digipolis/sbascstreetlights/v1',
  //   getAuthHdr: getAntwerpPilotsToken,
  //   toValidate: ['ngsiApi']
  // }
  Antwerp :  { 
    contextData : 'https://ext-api-gw-p.antwerpen.be/sirus/orion-admin-api-a/v2',
    getAuthHdr: getAntwerpToken,
    toValidate: ['dataModels']
  }

}


