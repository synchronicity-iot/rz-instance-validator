const log = require('./logger');
const axios = require('axios');

const getToken = async () => {
  return {'Authorization': 'Basic bm9pc2VhYmlsaXR5OkF0b3MyMDE5IQ=='};
};


module.exports = {
  EindhovenNoiseability :  { 
    contextData : 'https://orion.my-city.org/noiseability/v2',
    getAuthHdr: getToken,
    dmsValidation: { 
      fiwareServices: [''], 
      fiwareServicePaths: [''],
    },
    toValidate: ['dataModels']
  },
  // EindhovenRainbrian :  { 
  //   contextData : 'https://orion.my-city.org/noiseability/v2',
  //   getAuthHdr: getToken,
  //   toValidate: ['ngsiApi']
  // }

}
