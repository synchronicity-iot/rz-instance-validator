const log = require('./logger');
const axios = require('axios');

const getHelsinkiToken = async () => {
  const options = {
    method: 'post',
    url: 'https://synchronicity.cs.hut.fi/oauth2/token',
    data: 'grant_type=client_credentials&client_id=3fb4388d-0ba4-42b5-a7ee-3f82d1781bed&client_secret=38b0ef73-b664-4b77-87bf-c756a9d8161c',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  };
  let res = await axios(options).catch (e=> {console.log(e.response.data)});
  if (res) {
    return {'Authorization': 'Bearer ' + res.data['access_token']};
  } else {
    return undefined
  }
};

const getNullToken = async () => {
  return {};
};


module.exports = {
  Helsinki :  { 
    contextData : 'https://synchronicity.cs.hut.fi/orion/v2',
    getAuthHdr: getHelsinkiToken,
    dmsValidation: [{ 
      endpoint : 'https://synchronicity.cs.hut.fi/orion/v2',
      getAuthHdr: getNullToken,
      fiwareServices: [''], 
      fiwareServicePaths: ['']      
    }, {
      endpoint : 'https://ngsi.fvh.fi/v2/',
      getAuthHdr: getNullToken,
      fiwareServices: [''], 
      fiwareServicePaths: ['']
    }],
    toValidate: ['dataModels']
  }

}