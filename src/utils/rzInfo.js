const log = require('./logger');
const axios = require('axios');

const getNullToken = async () => {
  return {};
};

const getSantanderToken = async () => {
  const options = { 
    method: 'post',
    url: 'https://auth.san.synchronicity-iot.eu/oauth2/token',
    data: 'grant_type=password&username=testsync@synchronicity-iot.eu&password=telematica',
    headers: {
      'Authorization': 'Basic M2NlYjBkNjZiZWI5NGVhN2E1ZDU5OTNhNTQyZDE4MWQ6NzI3MTRlM2U1NTY1NDk2YTllYzViNzRkMjU2ZWVkODI=',
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  };
  let res = await axios(options).catch (e => {
    console.log(e.response.data)
  });
  return {'x-auth-token': res.data['access_token']};
};

const getMilanoToken = async () => {
  const options = {
    method: 'post',
    url: 'https://api.comune.milano.it:443/token',
    data: 'grant_type=password&username=ldiez&password=LfDf291751!',
    headers: {
      'Authorization': 'Basic ZUlBSUdKSzZmbWNGUnRMNktOTjdpdVpVTU84YTp1NjlQMVRmZHR2eFVSYllLaEVEZTZpeVNjQmdh',
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  };
  let res = await axios(options).catch (e => {
    console.log(e.response.data)
  });
  return {'Authorization': 'Bearer ' + res.data['access_token']};
}; 

const getCarougeToken = async () => {
  const options = {
    method: 'post',
    url: 'https://keyrock.cityreport.org:443/oauth2/token',
    data: 'grant_type=password&username=demo@mandint.org&password=d1mKYRCK#@',
    headers: {
      'Authorization': 'Basic MjU5MjJiM2YtN2ZjMS00YzcwLTgyODYtZTFiZGI5YzNhZWViOmE5NWNkY2VmLTY4YzMtNGUwOS04YWUxLTc0YzJmMWU3YWIyOQ==',
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  };
  let res = await axios(options);
  return {'x-auth-token': res.data['access_token']};
};

const getAntwerpToken = async () => {
  const options = {
    method: 'post',
    url: 'https://ext-api-gw-p.antwerpen.be/sirus/orion-consumer-api-a/v2/oauth2/token',
    data: 'grant_type=client_credentials&client_id=6aee2b05-4401-4ecf-8f3d-c5c2f29a49ac&client_secret=418ead9f-c315-4cc5-8424-678d77fd62e7', // for ORION
    // data: 'grant_type=client_credentials&client_id=d797bf5b-95ad-4d11-9750-529024d1bdf5&client_secret=35b2c9cd-2dad-4491-922a-1d6ff299938f', // for hist
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  };
  let res = await axios(options);
  console.log('Bearer ' + res.data['access_token'])
  return {'Authorization': 'Bearer ' + res.data['access_token']};
};

const getAntwerpPilotsToken = async () => {
  return {'apikey': 'b654a825-5296-41b0-8889-235e9f656b51'};
};

const getEindhovenPilotToken = async () => {
  return {'Authorization': 'Basic bm9pc2VhYmlsaXR5OkF0b3MyMDE5IQ=='};
};

const getCalatayudToken = async () => {
  const options = {
    method: 'post',
    url: 'http://idm-swm-calatayud.quamtra.com/oauth2/token',
    data: 'grant_type=password&username=mdelaasuncion@calatayud.es&password=Qu4mtr4.5WM',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization' :' Basic MmEwMzNkMjctYmZmNy00MGQwLWFjZWEtMzgxZjlmNGQ1ZTcxOjI5NDQ4OGY3LTk5YmUtNGU3NC1hNmEyLTMzZDg1ZDAwZmFiNQ=='
    }
  };
  let res = await axios(options).catch (e=> {console.log(e)});
  if (res) {
    return {'x-auth-token': res.data['access_token']};
  } else {
    return undefined
  }
};

const getNovisadToken = async () => {
  const options = {
    method: 'post',
    url: 'https://idm.synchronicity.dunavnet.eu/oauth2/token',
    data: 'grant_type=password&username=admin-idm-syn@dunavnet.eu&password=Dunavnet@123',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization' :' Basic YjM5YmM4MTktMmU4NS00NjQxLWI4NWMtNjU2NzJkM2M4ZDcxOmJjOTJhZDI3LWVlNmItNGQ4YS04MGRkLTQ2MTRjMzQ2Mzk4Zg=='
    }
  };
  let res = await axios(options).catch (e=> {console.log(e.response.data)});
  if (res) {
    return {'x-auth-token': res.data['access_token']};
  } else {
    return undefined
  }
};

// toValidate: ['dataModels', 'ngsiApi', 'historicalApi']
module.exports = {
  // getToken: getCalatayudToken, 
  Porto :  { 
    dmsValidation: [{ 
      endpoint : 'https://broker.fiware.urbanplatform.portodigital.pt/v2',
      getAuthHdr: getNullToken,
      fiwareServicePaths: [
        ['', '/']
      ],
    },{ 
      endpoint : 'http://ec2-18-185-130-23.eu-central-1.compute.amazonaws.com:1026/v2',
      getAuthHdr: getNullToken,
      fiwareServicePaths: [
        ['porto', '/']
      ],
    }],
    toValidate: ['dataModels'],
  },
  Santander : {
    dmsValidation: [{ 
      endpoint : 'https://context.san.synchronicity-iot.eu/v2',
      getAuthHdr: getSantanderToken,
      fiwareServicePaths: [
        [''           , '/'], 
        ['jabster'    , '/'], 
        ['ridespark'  , '/'], 
        ['kissmybike' , '/'], 
        ['kimap'      , '/'], 
        ['smartimpact', '/']
      ], 
    }, { 
      endpoint : 'https://test.synchronicity.ridespark.com:1026/v2',
      getAuthHdr: getNullToken,
      fiwareServicePaths: [
        ['santander', '/']
      ], 
    },{ 
      endpoint : 'http://ec2-18-185-130-23.eu-central-1.compute.amazonaws.com:1026/v2',
      getAuthHdr: getNullToken,
      fiwareServicePaths: [
        ['santander', '/']
      ], 
    }],
    toValidate: ['dataModels']
  },
  Antwerp :  { 
    dmsValidation: [{ 
      endpoint : 'https://ext-api-gw-p.antwerpen.be/sirus/orion-admin-api-a/v2',
      getAuthHdr: getAntwerpToken,
      fiwareServicePaths: [
        ['',                                        '/'],
        ['vmm_airquality_vmm',                      '/'], 
        ['stadantwerpen_wastecontainer_bigbelly',   '/'], 
        ['stadantwerpen_wo_cutler',                 '/'], 
        ['digipolis_parking_digipark',              '/'], 
        ['mpa_laadloszones',                        '/'],
        ['',                                        '/gip'],
        ['vmm_airquality_vmm',                      '/gip'], 
        ['stadantwerpen_wastecontainer_bigbelly',   '/gip'], 
        ['stadantwerpen_wo_cutler',                 '/gip'], 
        ['digipolis_parking_digipark',              '/gip'], 
        ['mpa_laadloszones',                        '/gip']
      ], 
    }, { 
      endpoint : 'https://ext-api-gw-p.antwerpen.be/digipolis/sbascactivetravelis/v1',
      getAuthHdr: getAntwerpPilotsToken,
      fiwareServicePaths: [
        ['', '/']
      ]
    }, { 
      endpoint : 'https://ext-api-gw-p.antwerpen.be/digipolis/sbascencouragingcyc/v1',
      getAuthHdr: getAntwerpPilotsToken,
      fiwareServicePaths: [
        ['', '/']
      ]
    }, { 
      endpoint : 'https://ext-api-gw-p.antwerpen.be/digipolis/sbasckissmybike/v1',
      getAuthHdr: getAntwerpPilotsToken,
      fiwareServicePaths: [
        ['', '/']
      ]
    }, { 
      endpoint : 'https://ext-api-gw-p.antwerpen.be/digipolis/sbascrainbrain/v1',
      getAuthHdr: getAntwerpPilotsToken,
      fiwareServicePaths: [
        ['', '/']
      ]
    }, { 
      endpoint : 'https://ext-api-gw-p.antwerpen.be/digipolis/sbascstreetlights/v1',
      getAuthHdr: getAntwerpPilotsToken,
      fiwareServicePaths: [
        ['', '/']
      ]
    }],
    toValidate: ['dataModels']
  },
  Carouge :  {
    dmsValidation: [{ 
      endpoint : 'https://orion.cityreport.org:5005/v2',
      getAuthHdr: getCarougeToken,
      fiwareServicePaths: [
        ['carouge', '/OnStreetParking'],
        ['carouge', '/OffStreetParking'],
        ['carouge', '/NoiseLevelObserved'],
        ['carouge', '/Vehicle'],
        ['carouge', '/TrafficFlowObserved'],
        ['carouge', '/AirQualityObservedPM10'],
        ['carouge', '/AirQualityObservedNO2'],
        ['carouge', '/AirQualityObservedO3'],
        ['carouge', '/ParkingSpot']
      ],
    }],
    toValidate: ['dataModels']
  },
  Eindhoven :  {
    dmsValidation: [{
      endpoint : 'https://orion.my-city.org/rainbrain/v2',
      getAuthHdr: getEindhovenPilotToken,
      fiwareServicePaths: [
        ['', '/']
      ],
    }, { 
      endpoint : 'https://orion.my-city.org/noiseability/v2',
      getAuthHdr: getEindhovenPilotToken,
      fiwareServicePaths: [
        ['', '/']
      ],
    }],
    toValidate: ['dataModels']
  },
  Helsinki :  {
    dmsValidation: [{ 
      endpoint : 'https://synchronicity.cs.hut.fi/orion/v2',
      getAuthHdr: getNullToken,
      fiwareServicePaths: [
        ['', '/']
      ]      
    }, {
      endpoint : 'https://ngsi.fvh.fi/v2/',
      getAuthHdr: getNullToken,
      fiwareServicePaths: [
        ['', '/']
      ]
    }],
    toValidate: ['dataModels']
  },
  Manchester :  { 
    dmsValidation: [{ 
      endpoint : 'http://ec2-18-185-130-23.eu-central-1.compute.amazonaws.com:1026/v2',
      getAuthHdr: getNullToken,
      fiwareServices: [], 
      fiwareServicePaths: [
        ['manchester', '/']
      ],
    }],
    toValidate: []
  },
  Milan :  {
    dmsValidation: [{ 
      endpoint : 'https://api.comune.milano.it/synchronicity/context/1.0/v2',
      getAuthHdr: getMilanoToken,
      fiwareServicePaths: [
        ['',            '/'], 
        ['milan',       '/AirQualityObserved'], 
        ['milan',       '/WeatherObserved'], 
        ['milan',       '/UrbanMobility'],
        ['milan',       '/Linc'],
        ['milan',       '/KiunsysMonitor'],
        ['milan',       '/QueueMonitor'],
        ['kissmybike',  '/Vehicle'],
        ['kimap',       '/'],
    ],
    }],
    toValidate: ['dataModels']
  },
  Tampere : {
    dmsValidation: [],
    toValidate: ['']
  },
  Lanucia : {
    dmsValidation: [{ 
      endpoint : 'https://test.synchronicity.ridespark.com:1026/v2',
      getAuthHdr: getNullToken,
      fiwareServicePaths: [
        ['lanucia', '/']
      ],
    }],
    toValidate: ['dataModels']
  },
  Herning : {
    dmsValidation: [{ 
      endpoint : 'http://ec2-18-185-130-23.eu-central-1.compute.amazonaws.com:1026/v2',
      getAuthHdr: getNullToken,
      fiwareServicePaths: [
        ['herning', '/']
      ],
    }],
    toValidate: ['dataModels']
  },
  Bilbao : {
    dmsValidation: [{ 
      endpoint: 'http://synchronicity.bilbao.usmart.io:1026/v2',
      getAuthHdr: getNullToken,
      fiwareServicePaths: [
        ['', '/']
      ],
    }],
    toValidate: ['dataModels']
  },
  Edinburgh : {
    dmsValidation: [{ 
      endpoint: 'http://edi-sync-lb-1271070260.eu-west-1.elb.amazonaws.com/v2',
      getAuthHdr: getNullToken,
      fiwareServicePaths: [
        ['', '/']
      ],
    }],
    toValidate: ['dataModels']
  },
  Calatayud : {
    dmsValidation: [{
      endpoint : 'http://ocb-swm-calatayud.quamtra.com/v2',
      getAuthHdr: getCalatayudToken,
      fiwareServicePaths: [
        ['calatayud', '/garbage']
      ],
    }],
    toValidate: ['dataModels']
  },
  Novisad : {
    dmsValidation: [{
      endpoint: 'https://cb.synchronicity.dunavnet.eu/v2',
      getAuthHdr: getNovisadToken,      
      fiwareServicePaths: [
        ['', '/']
      ]
    }],
    toValidate: ['dataModels']
  },
  Donegal : {
    dmsValidation: [],
    toValidate: ['']
  }
}
