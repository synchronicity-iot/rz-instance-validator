const _ = require('lodash');
const ngsiApi = require('../apis/ngsiApi');
const historicalApi = require('../apis/historicalApi');
const checkEnts = require('./checkEnts');
const step = require('../../config').queryStep;
const limit = require('../../config').queryLimit;
const log = require('../utils/logger');
const getErrorMsg = require('../utils/errors').getErrorMsg;
const sleep = require('sleep');

const fetchEnts = async (opt, type, offset) => {
  log.info ('Validating from ' + offset + ' to ' + (offset+step));
  const res = await ngsiApi.getEntitiesByType(opt, type, offset, step);

  try {
    return checkEnts.batchCheck(res, type);
  } catch (e) {
    log.error(e)
    // console.log(e)
  }
};

const entitiesByType = async (opt, type, v = false) => {
  const ret = {
    total: 0,
    success: 0,
    fail: 0, 
    detail: []
  };
  sleep.msleep(2000)
  const t = await ngsiApi.getOneType(opt, type);
  const count = t.count > limit ? limit : t.count;
  ret.total = count;
  log.info (count + ' entities of type ' + type)
  const offsets = _.range(0, count, step);
  
  for (const offset of offsets ) {
    sleep.msleep(2000)
    const aux = await fetchEnts (opt, type, offset);
    if (aux.success === -1) {
      ret.success = 0;
      ret.fail = count;
      ret.detail = aux.detail;
      return ret; 
    } else {
      ret.success += aux.success;
      ret.fail += aux.fail;
      if (v && offset == 0) {
        ret.detail = _.chunk(aux.detail, 5)[0];
      } 
    }
  }
  
  return ret;
};

const ngsiApiVal = async (opt) => {
  let ret = {};
  // Just in case it is there 
  log.info ("=== Deleting entity")
  await ngsiApi.deleteEntity (opt).catch(getErrorMsg);
  sleep.msleep(1000)
  log.info ("=== Creating entity")
  ret.createEntity = await ngsiApi.createEntity (opt).catch (getErrorMsg);
  sleep.msleep(1000)
  log.info ("=== Reading entity")
  ret.readEntity = await ngsiApi.queryEntity (opt).catch (getErrorMsg);
  sleep.msleep(1000)
  log.info ("=== queryEntityAttr")
  ret.readEntityAttr = await ngsiApi.queryEntityAttr (opt).catch (getErrorMsg);
  sleep.msleep(1000)
  log.info ("=== queryEntityAttrVal")
  ret.readEntityAttrValue = await ngsiApi.queryEntityAttrVal (opt).catch (getErrorMsg);
  sleep.msleep(1000)
  log.info ("=== findEntityByType")
  ret.findEntityByType = await ngsiApi.findEntityByType (opt).catch (getErrorMsg);
  sleep.msleep(1000)
  log.info ("=== findEntityByIdpattern")
  ret.findEntityByIdpattern = await ngsiApi.findEntityByIdpattern (opt).catch (getErrorMsg);
  sleep.msleep(1000)
  log.info ("=== findEntityByAttrvalue")
  ret.findEntityByAttrvalue = await ngsiApi.findEntityByAttrvalue (opt).catch (getErrorMsg);
  sleep.msleep(1000)
  log.info ("=== updateEntityAndCheck")
  ret.updateEntity = await ngsiApi.updateEntityAndCheck (opt).catch (getErrorMsg);
  sleep.msleep(1000)
  log.info ("=== deleteEntity")
  ret.deleteEntity = await ngsiApi.deleteEntity (opt).catch (getErrorMsg);
  sleep.msleep(1000)
  log.info ("=== createSubscription")
  ret.createSubscription = await ngsiApi.createSubscription (opt).catch (getErrorMsg);
  sleep.msleep(1000)
  log.info ("=== deleteSubscription")
  ret.deleteSubscription = await ngsiApi.deleteSubscription (opt).catch (getErrorMsg);
  sleep.msleep(1000)
  return ret;
};

const historicalApiVal = async (opt, entity, attribute) => {
  let ret = {};
  ret.afterLastn = await historicalApi.getAfterLastn(opt, entity, attribute).catch (getErrorMsg);
  ret.beforeLastn = await historicalApi.getBeforeLastn(opt, entity, attribute).catch (getErrorMsg);
  ret.betweenLastn = await historicalApi.getBetweenLastn(opt, entity, attribute).catch (getErrorMsg);
  ret.paging = await historicalApi.getPaging(opt, entity, attribute).catch (getErrorMsg);
  ret.afterPaging = await historicalApi.getAfterPaging(opt, entity, attribute).catch (getErrorMsg);
  ret.beforePaging = await historicalApi.getBeforePaging(opt, entity, attribute).catch (getErrorMsg);
  ret.betweenPaging = await historicalApi.getBetweenPaging(opt, entity, attribute).catch (getErrorMsg);
  log.info(JSON.stringify(ret, null, 4))
  return ret;
} 

module.exports = { 
  entitiesByType: entitiesByType,
  ngsiApi : ngsiApiVal, 	
  historicalApi : historicalApiVal
};