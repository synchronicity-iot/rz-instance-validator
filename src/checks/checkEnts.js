const _ = require('lodash');
const fs = require('fs-extra');
const find = require('find');
const log = require('../utils/logger');
const axios = require('axios');
const Ajv = require('ajv');

const findFiles = async (folder) => {
  return new Promise( (resolve, reject) => {
    find.file ('schema.json', folder, (files) => {
      return resolve (files)
    }).error (()=> {
      return reject();
    })
  });
}

const adaptDraft = (obj) => {
  obj['$schema'] = 'http://json-schema.org/draft-07/schema';
  return obj;
}

const getType = (fn) => {
  const strs = _.split(fn, '/');
  const type = strs[strs.length-2];
  return type;
}

const loadSchema = async (uri) => {
  log.info('Loading remote schema ' + uri);
  const dmJson = await axios.get(uri).catch (e => {
    log.error('Compile async error');
    console.log(e.data);
  }) 
  return dmJson.data;
}

var ajv = new Ajv({ allErrors: true , schemaId: '$id', loadSchema: loadSchema});
// ajv.addMetaSchema(require('ajv/lib/refs/json-schema-draft-07.json'));

const addSchema = (obj, key) => {
  try {
    obj = adaptDraft(obj);
    if (key !== undefined) {
      ajv.addSchema(obj, key);
    } else {
      ajv.addSchema(obj);
    }
  } catch (e) {
    log.error ('Error while adding schema ' + (key?key:'unnamed'))
    console.log(e)
  }
}

const clearSchemas = () => {
  ajv.removeSchema();
}

const findDmPath = (dm, files) => {
  for (const file of files){
    const type = getType(file);
    if (dm === type){
      return file;
    }
  }
  return null;
}

const buildDmStatus = async (dms) => {
  const files = await findFiles('./DataModels');
  const fullDms = {};
  
  for (const dm of dms) {
    const dmName = dm['Data Model']
    const fnPath = findDmPath(dmName, files);

    fullDms[dmName] = {
      status : dm['Approval Status'],
      source : dm['Original Source'],
      description: dm['Description'],
      vertical: dm['Vertical'],
      path : fnPath
    }
  }
  return fullDms;
}

const patchSchema = (obj) => {
  try {
  //   if (obj.allOf[2].properties['type'].enum[0] == 'OffStreetParking'){
  //     obj.allOf[2].properties['maximumAllowedHeight'].exclusiveMinimum = false;
  //     obj.allOf[2].properties['averageSpotWidth'].exclusiveMinimum = false;
  //     obj.allOf[2].properties['averageSpotLength'].exclusiveMinimum = false;
  //     obj.allOf[2].properties['maximumAllowedWidth'].exclusiveMinimum = false;
    // }
    if (obj.allOf[2].properties['type'].enum[0] == 'EVChargingStation'){
      obj.allOf[2].properties['amperage'].exclusiveMinimum = obj.allOf[2].properties['amperage'].minimum;
      obj.allOf[2].properties['voltage'].exclusiveMinimum = obj.allOf[2].properties['voltage'].minimum;
    }
  } catch (e) {}
  return obj;
}

const adaptSchemaId = (obj, key = undefined) => {
  if (_.has(obj, 'id')){
    obj['$id'] = obj['id'];
    obj = _.omit(obj, 'id')
  }
  if (key !== undefined){
    obj =  patchSchema(obj);
  }
  return obj;
}

const addCommonSchema = async (filename) => {
  log.info('Adding schema ' + filename)
  var obj = await JSON.parse(fs.readFileSync(filename, 'utf8'));
  obj = adaptSchemaId(obj)
  addSchema(obj);
}

const setupDataModels = async (dms) => {
  clearSchemas();
  const fullDms = await buildDmStatus(dms);

  await addCommonSchema('DataModels/common-schema.json')
  await addCommonSchema('DataModels/geometry-schema.json')
  await addCommonSchema('DataModels/AgriFood/agrifood-schema.json')
  await addCommonSchema('DataModels/Device/device-schema.json')  
  await addCommonSchema('DataModels/UrbanMobility/gtfs-schema.json')
  await addCommonSchema('DataModels/Weather/weather-schema.json')
  let dmsCtr = 0;
  _.forOwn(fullDms, async (value, key) => {
    log.info('Adding DataModel -->\t' + key);
    if (value.path)  { //value.status === 'Approved') {
      try {
        var obj = JSON.parse(fs.readFileSync(value.path, 'utf8'));
        obj = adaptSchemaId(obj, key)
        addSchema(obj, key);
        ++dmsCtr;
      } catch (e) {console.log(e)}
  
    } else {
      log.warn ('JSON schema not specified and status '+ value.status)
    }
  });
  log.info ('# of compiled datamodels ' + dmsCtr);
}

const validate = async (data, type) => {
	if (!_.has(ajv._schemas, type)){
    log.error ('Type ' + type + ' is not registered')
		return {status: 'TYPE_ERROR', detail: 'Not registered type'}; 		
	}
  try {
    var v = ajv.getSchema(type);
    var valid = v(data)
  } catch(e) {
    log.error('There was an error');
    console.log(e);
    return {status: 'TYPE_ERROR', detail: 'Not registered type'}; 		
  };
	if (valid){
    return {status: 'ENTITY_VALID'}; 		
	} else {
		let aux = v.errors;
		aux = _.map(aux, (o) => {
      return _.omit(o, ['schemaPath', 'keyword']);
    });
		return {status: 'ENTITY_ERROR', entityId: data.id, detail: aux};
	}
};

const batchCheck = async (data, type) => {
	let ret = {
		success: 0,
		fail: 0,
		detail: []
  };
  var i = 0;
  for (const d of data) {
    
    ++i;
    const aux = await validate(d, type);
    if (aux.status === 'TYPE_ERROR') {
      ret.fail = -1;
      ret.success = -1;
      ret.detail = [aux.detail];
      return ret;
    }
		else if (aux.status === 'ENTITY_ERROR') {
      ret.fail += 1;
			ret.detail.push(aux);
		} else {
			ret.success += 1;
    }
  }
	return ret;	
};

const getSchema = (type) => {
  return ajv._schemas[type]
}
const getSchemaNames = () => {
  let keys = _.keys(ajv._schemas);
  keys = _.filter(keys, e => {
    return !e.startsWith('http');
  })
  return keys;
}

module.exports = {
  batchCheck: batchCheck,
  validate: validate,
  recompileDms: setupDataModels,
  getSchema: getSchema,
  getSchemaNames: getSchemaNames
};