const _ = require('lodash'); 
const moment = require('moment');
const axios = require('axios');
const log = require('../utils/logger');

const getOptions = (opt, entity, attribute) => {
  return {
    method: 'get',
    url: opt.url+'/entities/' + entity.id + '/attrs/' + attribute + (entity.type? ('?type='+entity.type) : ''),
    headers: opt.headers
  }    
};

const cleanDates = (res, attribute) => {
    return _.map(res.data[attribute], 'modifiedAt');
};

const getFirstRecordDate = async (opt, entity, attribute) => {
  const options = getOptions(opt, entity, attribute);
  options.params = {
    limit: 1,
    offset: 0
  }
  const res = await axios(options);
  const dates = cleanDates(res, attribute);
  return dates[0];
}

const getLastRecordDate = async (opt, entity, attribute) => {
  const options = getOptions(opt, entity, attribute);
  options.params = {
    lastn: 1
  }
  const res = await axios(options);
  const dates = cleanDates(res, attribute);
  return dates[0];
}

/*
1.- Get overall first and last values
2.- Get lastn 5 records after first
3.- If it returns no or more than 5 records => error
4.- If maximum is different to last => error
5.- Get lastn 5 records after last
6.- If it returns more than one => error 
7.- If the only one is different to last => error
*/
const getAfterLastn = async (opt, entity, attribute) => {
  let last = await getLastRecordDate(opt, entity, attribute);
  let first = await getFirstRecordDate(opt, entity, attribute);
  const options = getOptions(opt, entity, attribute);
  options.params = {
    timerel: 'after',
    time: first,
    lastn: 5
  }
  let res = await axios(options);
  let dates = cleanDates (res, attribute);
  if ( dates.length === 0 || dates.length > 5) {
    return {status: false, msg: 'It returns no data or more than lastn'}
  }
  dates = _.map(dates, d => moment(d));
  const maxDate = moment.max(dates);
  if (  maxDate.diff(moment(last), 'seconds') !== 0 ) {
    return {status: false, msg: 'Newest value is not the last'}
  }
  options.params = {
    timerel: 'after',
    time: last,
    lastn: 5
  }
  res = await axios(options);
  
  dates = cleanDates (res, attribute);
  if ( dates.length > 1) {
    return {status: false, msg: 'It returns values after last'}
  }
  if ( dates.length === 1 && dates[0] !== last) {
    return {status: 'Error', msg: 'Oldest value is not last'}
  }
  return {status: 'ok'};
};

/*
1.- Get overall first and last values
2.- Get lastn 5 records before last
3.- If it returns no recrod or more than 5 => error
4.- If maximum is different to last => error
5.- Get lastn 5 recrods before first
6.- If it returns more than one record => errror
7.- If the only one is different to first => error
*/
const getBeforeLastn = async (opt, entity, attribute) => { 
  let last = await getLastRecordDate(opt, entity, attribute);
  let first = await getFirstRecordDate(opt, entity, attribute);
  const options = getOptions(opt, entity, attribute);
  ////////////////////////////////////////////////////////
  options.params = {
    timerel: 'before',
    time: last,
    lastn: 5
  }
  let res = await axios(options);
  let dates = cleanDates (res, attribute);
  if ( dates.length === 0 || dates.length > 5) {
    return {status: 'Error', msg: 'It returns no data or more than lastn'}
  }
  dates = _.map(dates, d => moment(d));
  const maxDate = moment.max(dates);
  if (  maxDate.diff(moment(last), 'seconds') !== 0 ) {
    return {status: 'Error', msg: 'Oldest value is not the last'}
  }
  ///////////////////////////////////////////////////////
  // options.params = {
    // timerel: 'before',
    // time: first,
    // lastn: 5
  // }
  // res = await axios(options);
  // dates = cleanDates (res, attribute);
  // if ( dates.length > 1) {
    // return {status: false, msg: 'It returns values after last'}
  // }
  // if ( dates.length === 1 && dates[0] !== first) {
    // return {status: false, msg: 'Newest value is not first'}
  // }
  ////////////////////////////////////////////////////////
  return {status: 'ok'};
}

/*
1.- Get overall first and last values
2.- Get lastn 5 records between first&last
3.- If it returns no recrod or more than 5 => error
4.- If maximum is different to last => error
*/
const getBetweenLastn = async (opt, entity, attribute) => { 
  let last = await getLastRecordDate(opt, entity, attribute);
  let first = await getFirstRecordDate(opt, entity, attribute);
  const options = getOptions(opt, entity, attribute);
  ////////////////////////////////////////////////////////
  options.params = {
    timerel: 'between',
    time: first,
    endTime: last,
    lastn: 5
  }
  let res = await axios(options);
  let dates = cleanDates (res, attribute);
  if ( dates.length === 0 || dates.length > 5) {
    return {status: false, msg: 'It returns no data or more than lastn'}
  }
  dates = _.map(dates, d => moment(d));
  const maxDate = moment.max(dates);
  if (  maxDate.diff(moment(last), 'seconds') !== 0 ) {
    return {status: false, msg: 'Oldest value is not the last'}
  }
  return {status: 'ok'};
}

/*
1.- ret1 <- Get offset 0 limit 2
2.- If size(ret1) > 2 => error
3.- ret2 <- Get offset 1 limit 1
4.- If size(ret2) > 1 => error
5.- if ret1[1] !== ret2[0] => error
*/
const getPaging = async (opt, entity, attribute) => { 
  const options = getOptions(opt, entity, attribute);
  options.params = {
    offset: 0,
    limit: 2
  }
  let res = await axios(options);
  let dates1 = cleanDates (res, attribute);
  if (dates1.length > 2) {
    return {status: false, msg: 'Limit not working properly'}
  }
  options.params = {
    offset: 1,
    limit: 1
  }
  res = await axios(options);
  let dates2 = cleanDates (res, attribute);
  if (dates1[1] !== dates2[0]) {
    return {status: false, msg: 'Paging not working properly'}
  }
  return {status: 'ok'};
}

/*
1.- [first, last] <- Get overll first and last
2.- ret1 <- Get after first offset 0 limit 2
3.- If size(ret1) > 2 => error
4.- If min(ret1) !== first => error
5.- ret2 <- Get after first offset 1 limit 1
6.- If ret1[1] != ret2[0] => error
*/
const getAfterPaging = async (opt, entity, attribute) => { 
  let last = await getLastRecordDate(opt, entity, attribute);
  let first = await getFirstRecordDate(opt, entity, attribute);
  const options = getOptions(opt, entity, attribute);
  ////////////////////////////////////////////////////////
  options.params = {
    timerel: 'after',
    time: first,
    limit: 2,
    offset: 0
  }
  let res = await axios(options);
  let dates1 = cleanDates (res, attribute);
  if ( dates1.length > 2) {
    return {status: false, msg: 'Limit not working properly'}
  }
  let dates = _.map(dates1, d => moment(d));
  const minDate = moment.min(dates);
  if (minDate.diff(moment(first), 'seconds') > 0) {
    return {status: false, msg: 'After not working properly with paging'}
  }

  options.params = {
    timerel: 'after',
    time: first,
    limit: 1,
    offset: 1
  }
  res = await axios(options);
  let dates2 = cleanDates (res, attribute);
  if ( dates2.length > 1) {
    return {status: false, msg: 'Limit not working properly'}
  }
  if (dates1[1] !== dates2[0]) {
    return {status: false, msg: 'Paging not working properly'}
  }
  return {status: 'ok'};
}

/*
1.- [first, last] <- Get overll first and last
2.- ret1 <- Get before last offset 0 limit 2
3.- If size(ret1) > 2 => error
4.- If min(ret1) !== first => error
5.- ret2 <- Get before last offset 1 limit 1
6.- If ret1[1] != ret2[0] => error
*/
const getBeforePaging = async (opt, entity, attribute) => { 
  let last = await getLastRecordDate(opt, entity, attribute);
  let first = await getFirstRecordDate(opt, entity, attribute);
  const options = getOptions(opt, entity, attribute);

  options.params = {
    timerel: 'before',
    time: last,
    limit: 2,
    offset: 0
  }
  let res = await axios(options);
  let dates1 = cleanDates (res, attribute);
  if ( dates1.length > 2) {
    return {status: false, msg: 'Limit not working properly'}
  }
  let dates = _.map(dates1, d => moment(d));
  const minDate = moment.min(dates);
  if (minDate.diff(moment(first), 'seconds') > 0) {
    return {status: false, msg: 'After not working properly'}
  }

  options.params = {
    timerel: 'before',
    time: last,
    limit: 1,
    offset: 1
  }
  res = await axios(options);
  let dates2 = cleanDates (res, attribute);
  if ( dates2.length > 1) {
    return {status: false, msg: 'Limit not working properly'}
  }
  if (dates1[1] !== dates2[0]) {
    return {status: false, msg: 'Paging not working properly'}
  }
  return {status: 'ok'}; 
}

/*
1.- [first, last] <- Get overll first and last
2.- ret1 <- Get between first&last offset 0 limit 2
3.- If size(ret1) > 2 => error 
4.- If min(ret1) !== first => error
5.- ret2 <- Get before last offset 1 limit 1
6.- If ret1[1] != ret2[0] => error
*/
const getBetweenPaging = async (opt, entity, attribute) => { 
  let last = await getLastRecordDate(opt, entity, attribute);
  let first = await getFirstRecordDate(opt, entity, attribute);
  const options = getOptions(opt, entity, attribute);

  options.params = {
    timerel: 'between',
    time: first,
    endTime: last,
    limit: 2,
    offset: 0
  }
  let res = await axios(options);
  let dates1 = cleanDates (res, attribute);
  if ( dates1.length > 2) {
    return {status: false, msg: 'Limit not working properly'}
  }
  let dates = _.map(dates1, d => moment(d));
  const minDate = moment.min(dates);
  if (minDate.diff(moment(first), 'seconds') > 0) {
    return {status: false, msg: 'After not working properly'}
  }

  options.params = {
    timerel: 'between',
    time: first,
    endTime: last,
    limit: 1,
    offset: 1
  }
  res = await axios(options);
  let dates2 = cleanDates (res, attribute);
  if ( dates2.length > 1) {
    return {status: false, msg: 'Limit not working properly'}
  }
  if (dates1[1] !== dates2[0]) {
    return {status: false, msg: 'Paging not working properly'}
  }
  return {status: 'ok'};
}

module.exports = {
  getAfterLastn: getAfterLastn,
  getBeforeLastn: getBeforeLastn,
  getBetweenLastn: getBetweenLastn,
  getPaging: getPaging,
  getAfterPaging: getAfterPaging,
  getBeforePaging: getBeforePaging,
  getBetweenPaging: getBetweenPaging    
};
