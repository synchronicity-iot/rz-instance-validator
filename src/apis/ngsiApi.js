const _ = require('lodash'); 
const axios = require('axios');
const log = require('../utils/logger');
const sleep = require('sleep');
const timeout = 100000;

const getAllTypeNames = async (opt) => {
	const options = {
    method: 'get',
    url: opt.url+'/types?options=values',
    headers: opt.headers,
    timeout: timeout
  };
  const res = await axios(options)
  return res.data;
};

const getOneType = async (opt, type) => {
  const options = {
    method: 'get',
    url: opt.url+'/types/' + type,
    headers: opt.headers, 
    timeout: timeout
  };
  let res = await axios(options);
  return res.data;
};

const getEntitiesByType = async (opt, type, offset, limit) => {
	const options = {
    method: 'get',
		url: opt.url+'/entities?type=' + type + '&options=keyValues' + 
            (offset ? '&offset=' + offset : '') + 
            (limit ? '&limit=' + limit: '') 
            ,
		headers: opt.headers,
    timeout: timeout
  };
  let res = await axios(options);
  return res.data;
};

const testEntity = {
	id: "sc:apivalidation:entity",
	type: "test",
	attribute1: {
		value: 10,
		type: "Float"
	},
	attribute2: {
		value: "String attribute",
		type: "String"
	}
};

const createEntity = async (opt) => {
	const options = {
    method: 'post',
		url: opt.url+'/entities?type=' + testEntity.type,
		data: testEntity,
		headers: opt.headers,
    timeout: timeout
  };


  await axios(options).catch (e => {
    log.error('CreateEntity error');
    throw(e);
  });


  return {status: 'ok'};
}

const queryEntity = async (opt, retData = false) => {
	const options = {
    method: 'get',
		url: opt.url+'/entities/' + testEntity.id + '?type=' + testEntity.type,
		headers: opt.headers,
    timeout: timeout
  };

	await axios(options);
  return {status: 'ok'};
}

const queryEntityAttr = async (opt) => {
	const attr = 'attribute1';
	const options = {
    method: 'get',
		url: opt.url+'/entities/' + testEntity.id + '?type=' + testEntity.type + '&attrs=' + attr,
		headers: opt.headers,
    timeout: timeout
  };

	await axios(options);
  return {status: 'ok'};
}

const queryEntityAttrVal = async (opt) => {
	const attr = 'attribute1';
	const options = {
    method: 'get',
		url: opt.url+'/entities/' + testEntity.id + '?type=' + testEntity.type + '&attrs=' + attr + '&options=values',
		headers: opt.headers,
    timeout: timeout
  };

  await axios(options);
  return {status: 'ok'};
}

const findEntityByType = async (opt) => {
	const options = {
    method: 'get',
		url: opt.url+'/entities?type=' + testEntity.type,
		headers: opt.headers,
    timeout: timeout
  };

	const res = await axios(options);
	const val = res ? _.find(res.data, {'id': testEntity.id}) : res;
	if (val !== undefined) {
    return {status: 'ok'};
  } else {
    return {status: 'Reading error', msg: 'Entity ID is not the test entity' };
  }
}

const findEntityByIdpattern = async (opt) => {
	const pattern = 'sc:apivalidation:*';
	const options = {
    method: 'get',
		url: opt.url+'/entities?idPattern=' + pattern + '&type=' + testEntity.type,
		headers: opt.headers,
    timeout: timeout
  };

	const res = await axios(options);
	const val = res ? _.find(res.data, {'id': testEntity.id}) : res;
	if (val !== undefined) {
    return {status: 'ok'};
  } else {
    return {status: 'Reading error', msg: 'Entity ID is not the test entity' };
  }
}

const findEntityByAttrvalue = async (opt) => {
	const attr = 'attribute1';
	const options = {
    method: 'get',
		url: opt.url+'/entities?q=' + attr + '>1' + '&type=' + testEntity.type,
		headers: opt.headers,
    timeout: timeout
  };

  const res = await axios(options);
	const val = res ? _.find(res.data, {'id': testEntity.id}) : res;
	if (val !== undefined) {
    return {status: 'ok'};
  } else {
    return {status: 'Reading error', msg: 'Entity ID is not the test entity' };
  }
}

const updateEntityAndCheck = async (opt) => {
	const newAttr = {
		attribute1: {
			value: 8,
			type: 'Float'
		}
	};
	const options = {
    method: 'post',
		url: opt.url+'/entities/' + testEntity.id + '/attrs' + '?type=' + testEntity.type,
		data: newAttr,
		headers: opt.headers,
    timeout: timeout
  };
  
  await axios(options).catch (e => {
    log.error('UpdateEntity error') 
    throw(e);
  });
  options.method = 'get',
  options.url = opt.url+'/entities/' + testEntity.id + '?type=' + testEntity.type;
  delete options.data;
  sleep.msleep(1000)
	const res = await axios(options);
	if (!res || !res.data || res.data.attribute1.value !== newAttr.attribute1.value) {
		return {status: 'Updating error', msg: 'Attribute did not get updated'};
	}
  return {status: 'ok'};
}

const deleteEntity = async (opt) => {
  
	const options = {
    method: 'delete',
		url: opt.url+'/entities/' + testEntity.id + '?type=' + testEntity.type,
		headers: opt.headers,
    timeout: timeout
  };
  await axios(options).catch (e => {
    log.error('DeleteEntity error')
    throw(e)
  });
	return {status: 'ok'};
}

let subId = '';

const createSubscription = async (opt) => {
	const sub = {
		description: 'Subscription for SynchroniCity NGSI compliance',
		subject: {
			entities: [
				{
					id: testEntity.id,
					type: testEntity.type
				}
			]
		},
		condition: {
			attrs: ['attribute1']
		},
		notification: {
			http: {url: "http://localhost:1028/accumulate"},
			attrs: ['attribute1']
		},
		expires: "2040-01-01T14:00:00.00Z",
		throttling: 1
	};
	const options = {
    method: 'post',
		url: opt.url+'/subscriptions/',
		data: sub,
		headers: opt.headers,
    timeout: timeout
  };

	const res = await axios(options);
    if (res === undefined) {
      subId = '';
      return {status: 'Creation error', msg: 'Subscription ID is not returned'};
  }
  subId = res.headers.location.replace('/v2', ''),
  console.log(subId)
	return {status: 'ok'};
}

const deleteSubscription = async (opt) => {
	const options = {
    method: 'delete',
		url: opt.url + subId,
		headers: opt.headers,
    timeout: timeout
  };

	const res = await axios(options);
  subId = '';
  
	if (res !== undefined) {
    return {status: 'ok'};
  } 
  return {status: 'Deletion error', msg: ''};
}

module.exports = {
	getAllTypeNames: getAllTypeNames,
  getOneType: getOneType,		
	getEntitiesByType: getEntitiesByType, 
  createEntity: createEntity,
	queryEntity: queryEntity,
	queryEntityAttr: queryEntityAttr,
	queryEntityAttrVal: queryEntityAttrVal,
	findEntityByType: findEntityByType,
	findEntityByIdpattern: findEntityByIdpattern,
	findEntityByAttrvalue: findEntityByAttrvalue,
	updateEntityAndCheck: updateEntityAndCheck,
	deleteEntity: deleteEntity,
	createSubscription: createSubscription,
	deleteSubscription: deleteSubscription
};
