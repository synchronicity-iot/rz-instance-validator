var mongoose = require('mongoose')
var Schema = mongoose.Schema;

var StatusModel = new Schema({
  _id: {type: String},
  name: {type: String},
  lastUpdate: { type: Date},
  status: { type: Object, required: true },
});

module.exports = mongoose.model('StatusModel', StatusModel);
