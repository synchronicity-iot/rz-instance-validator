var mongoose = require('mongoose')
var Schema = mongoose.Schema;

var StatusModel = new Schema({
  status: { type: Object},
  service: { type: String},
  servicePath: { type: String},
  endpoint: { type: String},
  city: { type: String},
  lastUpdate: { type: Date},
});
StatusModel.index({city: 1, endpoint: 1, service: 1, servicePath: 1}, { unique: true });
module.exports = mongoose.model('datamodels', StatusModel);
