var mongoose = require('mongoose')
var Schema = mongoose.Schema;

var StatusModel = new Schema({
  _id: {type: String},
  status: { type: Object, required: true },
  lastUpdate: { type: Date},
});

module.exports = mongoose.model('ngsiApi', StatusModel);
