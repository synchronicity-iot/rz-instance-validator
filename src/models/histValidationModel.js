var mongoose = require('mongoose')
var Schema = mongoose.Schema;

var StatusModel = new Schema({
  _id: {type: String},
  lastUpdate:         { type: Date, required: true},
  entityId:           { type: String, required: true},
  attribute:          { type: String, required: true},
  entityType:         { type: String, required: true},
  fiwareService:      { type: String, required: true},
  fiwareServicePath:  { type: String, required: true},
  status:             { type: Object, required: true }
});

module.exports = mongoose.model('histApi', StatusModel);