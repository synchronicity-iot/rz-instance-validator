var config = {};
config.env = 'production'; // 'production' to store the logs in files
config.logLevel = 'silly'; // { error: 0, warn: 1, info: 2, verbose: 3, debug: 4, silly: 5 }
config.queryStep = 500; // number of entities request at the same time when validating data models
config.queryLimit = 100000; // number of entities validated for each type
config.httpPort = 8081;
config.httpsPort = 8443;
config.httpsCert = '/opt/letsencrypt/framework-validator.synchronicity-iot.smartsantander.eu.cert';
config.httpsKey = '/opt/letsencrypt/framework-validator.synchronicity-iot.smartsantander.eu.key';
config.dbAddr = 'localhost'; 
config.dbPort = 27017;
config.dbName = 'scMimValidator';
config.dataModelsRepo = 'https://gitlab.com/synchronicity-iot/synchronicity-data-models.git';
config.fwDataModelsRepo = 'https://github.com/Fiware/dataModels.git';

module.exports = config;

